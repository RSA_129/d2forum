from rest_framework.decorators import api_view
from rest_framework.response import Response
from base.models import Topic
from .serializers import TopicSerializer


@api_view(['GET'])
def getRountes(request):
    routes = [
        'GET /api',
        'GET /api/topics',
        'GET /api/topic/:pk',
    ]
    return Response(routes)


@api_view(['GET'])
def getTopics(request):
    topic = Topic.objects.all()
    ser = TopicSerializer(topic, many=True)
    return Response(ser.data)


@api_view(['GET'])
def getTopic(request, pk):
    topic = Topic.objects.get(id=pk)
    ser = TopicSerializer(topic, many=False)
    return Response(ser.data)
