from django.urls import path
from . import views

urlpatterns = [
    path('', views.getRountes),
    path('topics/', views.getTopics),
    path('topics/<int:pk>', views.getTopic),
]
