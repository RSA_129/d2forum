from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from .models import Topic, User


class TopicForm(ModelForm):
    class Meta:
        model = Topic
        fields = '__all__'
        exclude = ['host_user']


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['avatar', 'name', 'username', 'email', 'about']


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['avatar', 'name', 'username', 'email', 'password1', 'password2', 'about']


class EditUserForm(ModelForm):
    class Meta:
        model = User
        fields = ['avatar', 'name', 'about']

