from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db.models import Q, Count
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .models import Topic, Theme, Message, User
from .forms import TopicForm, UserForm, RegistrationForm


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        username = request.POST.get('username').lower()
        password = request.POST.get('password')
        if User.objects.filter(username=username).exists():
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.error(request, 'Incorrect password')
        else:
            messages.error(request, 'Incorrect login')
    context = {'page': 'login'}
    return render(request, 'base/reg_log.html', context)


def logoutUser(request):
    logout(request)
    return redirect('home')


def registerPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.username = user.username.lower()
            user.save()
            login(request, user)
            return redirect('home')
    context = {'page': 'register', 'form': RegistrationForm(request.POST or None)}
    return render(request, 'base/reg_log.html', context)


@login_required
def updateUser(request):
    user = request.user
    nxt = request.GET.get('next', '/')
    form = UserForm(instance=user)
    if request.method == 'POST':
        form = UserForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
            return redirect('profile', pk=user.id)
    context = {'form': form, 'next': nxt}
    return render(request, 'base/edit_user.html', context)


def home(request):
    search = request.GET.get('search') if request.GET.get('search') is not None else ''
    topics = Topic.objects.filter(
        Q(theme__name__icontains=search) |
        Q(name__icontains=search) |
        Q(description__icontains=search)
    )
    am_of_tops = topics.count()
    themes = Theme.objects.annotate(amount=Count('topic')).order_by('-amount').all()[:5]
    topic_messages = Message.objects.filter(Q(topic__theme__name__icontains=search))
    context = {'topics': topics,
               'themes': themes,
               'am_of_tops': am_of_tops,
               'fetched_messages': topic_messages,
               }
    return render(request, 'base/home.html', context)


def themesPage(request):
    search = request.GET.get('search') if request.GET.get('search') is not None else ''
    themes = Theme.objects.filter(Q(name__icontains=search)).annotate(amount=Count('topic')).order_by('-amount').all()
    context = {'themes': themes}
    return render(request, 'base/themes.html', context)


def activityPage(request):
    vse_messages = Message.objects.all()
    context = {'vse_messages': vse_messages}
    return render(request, 'base/messages.html', context)


def topic(request, pk):
    try:
        topic = Topic.objects.get(id=pk)
    except Topic.DoesNotExist:
        return redirect('home')
    topic_messages = topic.message_set.all().order_by('-created')
    if request.method == 'POST':
        Message.objects.create(
            user=request.user,
            topic=topic,
            body=request.POST.get('body'),
        )
        return redirect('topic', pk=topic.id)
    context = {'topic': topic, 'fetched_messages': topic_messages}
    return render(request, 'base/topic.html', context)


def userProfile(request, pk):
    try:
        user = User.objects.get(id=pk)
    except User.DoesNotExist:
        return redirect('home')
    topics = user.topic_set.all()
    user_messages = user.message_set.all()
    themes = Theme.objects.all()
    context = {'user': user, 'topics': topics, 'fetched_messages': user_messages, 'themes': themes}
    return render(request, 'base/profile.html', context)


@login_required
def createTopic(request):
    form = TopicForm()
    if request.method == 'POST':
        theme_name = request.POST.get('theme')
        theme, _ = Theme.objects.get_or_create(name=theme_name)
        Topic.objects.create(
            host_user=request.user,
            theme=theme,
            name=request.POST.get('name'),
            description=request.POST.get('description')
        )
        return redirect('home')
    themes = Theme.objects.all()
    context = {'form': form, 'themes': themes}
    return render(request, 'base/topic_form.html', context)


@login_required
def updateTopic(request, pk):
    topic = Topic.objects.get(id=pk)
    if request.user != topic.host_user:
        return HttpResponse("Access denied")

    form = TopicForm(instance=topic)
    if request.method == 'POST':
        theme_name = request.POST.get('theme')
        theme, _ = Theme.objects.get_or_create(name=theme_name)
        topic.name = request.POST.get('name')
        topic.theme = theme
        topic.description = request.POST.get('description')
        topic.save()
        return redirect('topic', pk=topic)
    themes = Theme.objects.all()
    context = {'form': form, 'themes': themes, 'topic': topic}
    return render(request, 'base/topic_form.html', context)


@login_required
def deleteTopic(request, pk):
    nxt = request.GET.get('next', '/')
    try:
        topic = Topic.objects.get(id=pk)
    except Topic.DoesNotExist:
        return redirect(nxt)

    if request.user != topic.host_user:
        return HttpResponse("Access denied")

    if request.method == 'POST':
        topic.delete()
        theme = topic.theme
        if not Topic.objects.filter(theme=theme.id).exists():
            theme.delete()
        return redirect('home')

    return render(request, 'base/delete.html', {'obj': topic, 'next': nxt})


@login_required
def deleteMessage(request, pk):
    nxt = request.GET.get('next', '/')
    if not Message.objects.filter(id=pk).exists():
        return redirect(nxt)
    mes = Message.objects.get(id=pk)
    if request.user != mes.user:
        return HttpResponse("Access denied")
    if request.method == 'POST':
        mes.delete()
        return redirect(nxt)

    return render(request, 'base/delete.html', {'obj': mes, 'next': nxt})


