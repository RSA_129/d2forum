from django.urls import path
from main_folder.settings import LOGIN_URL

from . import views

urlpatterns = [
    path(f'{LOGIN_URL}/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path(f'register/', views.registerPage, name='register'),

    path('', views.home, name='home'),
    path('topic/<int:pk>/', views.topic, name='topic'),
    path('themes/', views.themesPage, name='themes'),
    path('messages/', views.activityPage, name='messages'),
    path('profile/<int:pk>/', views.userProfile, name='profile'),


    path('create-topic/', views.createTopic, name='create_topic'),
    path('update-topic/<int:pk>/', views.updateTopic, name='update_topic'),
    path('delete-topic/<int:pk>/', views.deleteTopic, name='delete_topic'),
    path('delete-message/<int:pk>/', views.deleteMessage, name='delete_message'),

    path('edit-user/', views.updateUser, name='edit_user'),

]
