from django.contrib import admin

# Register your models here.
from .models import Topic, Theme, Message, User

admin.site.register(Topic)
admin.site.register(Theme)
admin.site.register(Message)
admin.site.register(User)
